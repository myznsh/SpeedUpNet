package util

// 打开系统默认浏览器

import (
	"os/exec"
	"runtime"
)

var commands = map[string]string{
	"windows": "start",
	"darwin":  "open",
	"linux":   "xdg-open",
}

func OpenUrl(uri string) {
	system := runtime.GOOS
	switch system {
	case "windows":
		{
			// 无GUI调用
			exec.Command(`cmd`, `/c`, `start`, uri).Start()
			break
		}
	case "linux":
		{
			exec.Command(`xdg-open`, uri).Start()
			break
		}
	case "darwin":
		{
			exec.Command(`open`, uri).Start()
			break
		}
	}
}
