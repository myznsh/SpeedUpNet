# SpeedUpNet
SpeedUpNet更新最快的IP映射HOST,从而提高网站的访问速度。经常遇到访问一些网站显示内容里图片无法正常加载，比如github最近头像，介绍
图片等无法加载问题，甚至有部分网站都无法进入。本文介绍一款软件，一键突破或加速网站访问。
原理是找出最快的服务器IP映射HOSTS。

## 相关框架
https://fyne.io/develop/cross-compiling.html  
https://goframe.org

## 下载二进制执行文件
https://gitee.com/dwxdfhx/SpeedUpNet/tree/master/build

## 软件界面
   首页  
   ![首页](images/1.png)  
   执行结果  
   ![执行结果](images/2.png)
   执行结果  
   ![执行结果](images/3.png)
## 常用命令
```golang
go mod download
go mod vendor
go get github.com/lucor/fyne-cross
fyne-cross --targets=linux/amd64,windows/amd64,darwin/amd64
```

### 更新日志
2020年2月28日
* 查看hosts功能
* bug fix
* ui update
* 版本检测更新

